#include "matching.h"
#include "stdafx.h"
#include "json.h"
using namespace std;

void swap(int *a, int *b)
{
	int *t;
	t = a;
	a = b;
	b = t;
}

//部门候选学生名单
int departments_students[1050][205] = { 0 };
//部门招生热度
int departments_heat[1050] = { 0 };
//学生报名部门
int students_departments[5005][7] = { 0 }; /*[][5]用于记录选择部门*/
 //部门hash表
string hashmap[1050];
//部门招收情况表
int department_change[1005] = { 0 };
int department_flag[105] = { 0 };
//学生意愿热度
int students_wishmax[5005][2] = { 0 };
//部门人数
int department_number[105] = { 0 };

//堆维护
void maxsmaintain(int A[], int length, int i )
{
	int left = i * 2, right = i * 2 + 1, max = students_wishmax[i][0], largest = i;
	if (left < length && max < students_wishmax[left][0])
	{
		largest = left, max = students_wishmax[left][0];
	}
	if (right < length && max < students_wishmax[right][0])
	{
		largest = left, max = students_wishmax[right][0];
	}
	if (largest != i)
	{
		swap(A[largest], A[i]);
		maxsmaintain(A, length, largest);
	}
}

unsigned int BKDRHash(char *str)
{
	unsigned int seed = 131313; // 31 131 1313 13131 131313 etc..
	unsigned int hash = 0;

	while (*str)
	{
		hash = hash * seed + (*str++);
	}

	return (hash & 0x7FFFFFFF);
}


unsigned int dNameHash(string s)
{
	char *str = (char*)s.data();
	char jd = 0;
	unsigned int key;
	string again[5] = { "National Association for blowing music"
		,"DV Society"
		,"Table tennis association"
		,"Wushu Association"
		,"Tennis association" };
	for (int i = 0; i<5; i++)
	{
		if (again[i] == s)
		{
			jd = i + 1;
		}
	}

	if (jd)
	{
		key = (3 + jd);
	}
	else key = BKDRHash(str);
	unsigned int where = key % 1000;
	return where;
}
/*
int timeISok(int stu_t[7][7], string d_t[], int size)
{
	for (int i = 0; i<size; i++)
	{
		string w = d_t[i];
		int wi;
		if (w[0] == 'M')	wi = 0;
		else if (w[0] == 'T')
		{
			if (w[1] == 'u')	wi = 1;
			else if (w[1] = 'h')	wi = 3;
			else;
		}
		else if (w[0] == 'W')	wi = 2;
		else if (w[0] == 'F')	wi = 4;
		else if (w[0] == 'S') {
			if (w[1] == 'a')	wi = 5;
			else if (w[1] == 'u')	wi = 6;
			else;
		}
		int begin = int(w[4] - '0') * 10 + int(w[5] - '0'), end = int(w[10] - '0') * 10 + int(w[11] - '0');
		for (int j = begin + 1; j<end; j = j + 2)
		{
			if (stu_t[wi][(j - 8) / 2] == 0)
				return 0;
		}
	}
	return 1;
}

void sTimeAdd(string s_available_schedule[], int size, int s_time[7][7])
{
	for (int i = 0; i<size; i++)
	{
		string w = s_available_schedule[i];
		int wi;
		if (w[0] == 'M')	wi = 0;
		else if (w[0] == 'T')
		{
			if (w[1] == 'u')	wi = 1;
			else if (w[1] = 'h')	wi = 3;
			else;
		}
		else if (w[0] == 'W')	wi = 2;
		else if (w[0] == 'F')	wi = 4;
		else if (w[0] == 'S') {
			if (w[1] == 'a')	wi = 5;
			else if (w[1] == 'u')	wi = 6;
			else;
		}
		int begin = int(w[4] - '0') * 10 + int(w[5] - '0'), end = int(w[10] - '0') * 10 + int(w[11] - '0');
		for (int j = begin + 1; j<end; j = j + 2)
		{
			s_time[wi][(j - 8) / 2] = 1;
		}
	}
}
*/
int timeISok1(const char*t1, const char*t2)
{//t1为部门时间段，t2为学生时间段 
	char week1[3], week2[3];
	week1[0] = t1[0]; week1[1] = t1[1]; week1[2] = t1[2];
	week2[0] = t2[0]; week2[1] = t2[1]; week2[2] = t2[2];
	if (strcmp(week1, week2) == 0)
	{//比较是否在同一天 
		if ((int(t1[4]) * 10 + int(t1[5])) >= (int(t2[4]) * 10 + int(t2[5])))
		{//比较活动开始时间 
			if ((int(t1[10]) * 10 + int(t1[11])) <= (int(t2[10]) * 10 + int(t2[11])))
				return 1;//比较活动结束时间 
			else return 0;
		}
		else return 0;
	}
	else return 0;
}


int numOFsame1(string array1[], int size1, string array2[], int size2)
{
	int num = 0;
	bool swap = false;//标识变量，表示两种情况中的哪一种
	int end = size1;
	for (int i = 0; i < end;)
	{
		swap = false;//开始假设是第一种情况
		for (int j = 0; j < size2; j++) 
		{ //找到与该元素存在相同的元素，将这个相同的元素交换到与该元素相同下标的位置上
			if (timeISok1(array1[i].c_str(), array2[j].c_str()) == 0)
			{ //第二种情况，找到了相等的元素
				/*string tmp = array2[i];//对数组2进行交换
				array2[i] = array2[j];
				array2[j] = tmp;*/
				swap = true;//设置标志
				num++;
				break;
			}
		}
		/*if (swap != true)
		{ //第一种情况，没有相同元素存在时，将这个元素交换到尚未进行比较的尾部
			string tmp = array1[i];
			array1[i] = array1[--end];
			array1[end] = tmp;
		}
		else*/
			i++;
	}
	//结果就是在end表示之前的元素都找到了匹配，而end元素之后的元素都未被匹配
	return size1-num;
}

Json::Value heatmatching(Json::Value root)
{
	// 部门招生热度初始化
	for (int i = 0; i < (int)root["departments"].size(); i++)
	{
		int temp= dNameHash(root["departments"][i]["department_name"].asString());
		departments_heat[temp] = root["departments"][i]["member_limit"].asInt();
		department_change[temp] = i;
		department_number[i]=(int)root["departments"][i]["member_limit"].asInt();
	}

	// 联系学生与部门
	for (int i = 0; i < (int)root["students"].size(); i++)
	{
		for (int j = 0; j < (int)root["students"][i]["departments_wish"].size(); j++)
		{
			int temp = dNameHash(root["students"][i]["departments_wish"][j].asString());
			hashmap[temp] = root["students"][i]["departments_wish"][j].asString();
			string tempa[10];
			for (int t = 0; t < (int)root["departments"][department_change[temp]]["event_schedules"].size(); t++)
			{
				tempa[t] = root["departments"][department_change[temp]]["event_schedules"][t].asString();
			}
			string tempb[10];
			for (int t = 0; t < (int)root["students"][i]["available_schedule"].size(); t++)
			{
				tempb[t] = root["students"][i]["available_schedule"][t].asString();
			}
			if (!numOFsame1(tempa, (int)root["departments"][department_change[temp]]["event_schedules"].size(),tempb, root["students"][i]["available_schedule"].size()))
			{
				students_departments[i][j] = temp; //学生报名部门
				departments_students[temp][departments_students[temp][0] + 1] = i;//部门候选学生记录
				departments_students[temp][0]++;//记录该部门候选学生数
				//记录学生志愿中招生热度最高部门
				if (students_wishmax[i][0] < departments_heat[temp])
				{
					students_wishmax[i][0] = departments_heat[temp];
					students_wishmax[i][1] = temp;
				}
			}
		}
	}

		//学生最小堆实现

		//堆初始化
		int students_maxs[5005] = { 0 };
		for (int i = 1; i <= (int)root["students"].size(); i++)
		{
			students_maxs[i] = i - 1;
		}

		for (int i = root["students"].size() / 2; i >= 1; i--)
		{
			maxsmaintain(students_maxs, root["students"].size(), i);
		}

		//学生选部门
		int count = (int)root["students"].size();
		while (count > 0)
		{
			int chose_student = students_maxs[1];
			int chosen_department = students_wishmax[chose_student][1];
			if (students_wishmax[chose_student][0] == 0) //学生所有志愿部门皆满则无法加入任意部门
			{
				count = 0; break;
			}
			else
			{
				students_departments[chose_student][5] = chosen_department;
			}
			departments_heat[chosen_department]--;
			//联动更新
			for (int i = 0; i < departments_students[chosen_department][0]; i++)
			{
				int temp_student = departments_students[chosen_department][i + 1];
				if (temp_student != chose_student) // 如果候选学生不是已选学生，更改其中热度
				{
					if (students_wishmax[temp_student][1] == chosen_department)
					{
						students_wishmax[temp_student][0]--;
						for (int j = 0; j < (int)root["students"][temp_student]["departments_wish"].size(); j++)
						{
							int temp= dNameHash(root["students"][temp_student]["departments_wish"][j].asString());
							if (students_wishmax[temp_student][0] < departments_heat[temp])
							{
								students_wishmax[temp_student][0] = departments_heat[temp];
								students_wishmax[temp_student][1] = temp;
							}
	
						}
					}
				}
			}
			swap(students_maxs[1], students_maxs[count]);
			count--;
			for (int i = count/ 2; i >= 1; i--)
			{
				maxsmaintain(students_maxs, count, i);
			}
	}
	



	Json::Value root2;
	Json::Value matched_students;
	Json::Value matched_departments;
	Json::Value standalone_students;
	for (int i = 0; i < (int)root["students"].size(); i++)
	{
		if (students_departments[i][5] != 0)
		{
			matched_students["student"].append(i);
			if (department_flag[department_change[students_departments[i][5]]] == 0)
			{
				matched_departments["department"].append(hashmap[students_departments[i][5]]);
			}
	//		department_number[department_change[students_departments[i][5]]]--;
	//		matched_departments["department"].append(department_number[department_change[students_departments[i][5]]]);
			department_flag[department_change[students_departments[i][5]]] = 1;
		}
		else
		{
			standalone_students["student"].append(root["students"][i]["student_no"].asString()); 
		}
	}
	root2["standalone_students"].append(standalone_students);
	root2["matched_students"].append(matched_students);
	root2["matched_departments"].append(matched_departments);
	Json::Value standalone_departments;
	for (int i = 0; i < (int)root["departments"].size(); i++)
	{
		if (department_flag[i] == 0)
		{
			standalone_departments["department"].append(root["departments"][i]["department_name"].asString());
			//standalone_departments["department"].append(root["departments"][i]["member_limit"].asInt());
		}
	}
	root2["standalone_departments"].append(standalone_departments);
	return root2;
}
/* hash dNameHash(string s)*/



